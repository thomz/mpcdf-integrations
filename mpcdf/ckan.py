import os
from ckanapi import RemoteCKAN


class Client:
    def __init__(self):
        self.server = "https://reptor.mpcdf.mpg.de"

    def urlResource (self, dataset, url, name, description):
        try:
            ckan_token = os.environ["CKAN_TOKEN"]

            ua = 'ckanapiexample/1.0 (+http://example.com/my/website)'

            mysite = RemoteCKAN(self.server, apikey=ckan_token, user_agent=ua)
            mysite.action.resource_create(
                package_id=dataset,
                name=name,
                description=description,
                url=url
                )
            return True
        except:
            return False