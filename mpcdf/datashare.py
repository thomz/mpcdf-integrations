import requests


class Client:
    def __init__(self):
        self.server = "https://datashare.mpcdf.mpg.de"

    @staticmethod
    def download(sharelink, filename):
        try:
            r = requests.get(sharelink, allow_redirects=True)
            open(filename, 'wb').write(r.content)
            return True
        except requests.exceptions.RequestException:
            return False
