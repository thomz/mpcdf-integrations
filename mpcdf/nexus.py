import boto3
import os


class NexusS3:
    def __init__(self):
        self.server = "https://objectstore.hpccloud.mpcdf.mpg.de"

    def upload(self, uploadfile, bucket):
        try:
            s3 = boto3.client(
                's3',
                endpoint_url=self.server,
                aws_access_key_id=os.environ["NEXUS_S3_ACCESS"],
                aws_secret_access_key=os.environ["NEXUS_S3_SECRET"]
            )
            s3.upload_file(uploadfile, bucket, os.path.basename(uploadfile))
            return True
        except:
            return False