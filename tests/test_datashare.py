import unittest
from datetime import datetime
import os

import mpcdf.datashare as datashare


def test_datashare_download():
    current = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    dataFile = "dataFile-" + current + ".csv"
    ds = datashare.Client()
    ds.download("https://datashare.mpcdf.mpg.de/s/CzfBA2Hk62LA0ZD/download", dataFile)
    fileSize = os.stat(dataFile).st_size
    os.remove(dataFile)

    assert fileSize == 1547
